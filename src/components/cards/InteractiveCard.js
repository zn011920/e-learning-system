import { Box, Grid, Typography } from '@material-ui/core';
import { FlipCameraAndroid } from '@material-ui/icons';
import PropTypes from 'prop-types';
import React from 'react';
import Flippy, { BackSide, FrontSide } from 'react-flippy';

const InteractiveCard = ({ cards, styling }) => (
  <Grid container className="center-grid-container" spacing={2} wrap="wrap">
    {cards.map(((card) => (
      <Box sx={{ m: 1 }}>
        <Flippy key={card.front}>
          <FrontSide className={styling}>
            <Grid container className="flippy-card-container">
              <Grid item md={12} className="flippy-card-front">
                <Typography>{card.front}</Typography>
              </Grid>
              <Grid item className="flippy-card-icon">
                <FlipCameraAndroid color="action" />
              </Grid>
            </Grid>
          </FrontSide>
          <BackSide className={styling}>
            <Grid container className="flippy-card-container">
              <Grid item md={12} className="flippy-card-back">
                <Typography>{card.back}</Typography>
              </Grid>
              <Grid item className="flippy-card-icon">
                <FlipCameraAndroid color="action" />
              </Grid>
            </Grid>
          </BackSide>
        </Flippy>
      </Box>
    )))}
  </Grid>
);

InteractiveCard.propTypes = {
  cards: PropTypes.array.isRequired,
  styling: PropTypes.string.isRequired
};

export default InteractiveCard;
